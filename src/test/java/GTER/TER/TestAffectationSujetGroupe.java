package GTER.TER;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestAffectationSujetGroupe {
	Group g1 = new Group(1, null, null);
	Suj s1 = new Suj("test", g1);
	
	@Test
	public void testR1() {
		g1.setSujet(s1);
		assertTrue(g1.getSujetaffecté()==s1&&s1.getGroupe()==g1);
	}

}
