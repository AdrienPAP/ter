package GTER.TER;

public class Voe {
	private Suj sujet;
	private int priority;
	private Group groupe;
	
	
	
	public Voe(Suj sujet, int priority, Group groupe) {
		this.sujet = sujet;
		this.priority = priority;
		this.groupe = groupe;
	}

	public void setSujet(Suj s) {
		this.sujet = s;
	}

	public void setPriority(int i) {
		this.priority = i;
	}
}
