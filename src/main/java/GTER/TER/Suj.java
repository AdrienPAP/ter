package GTER.TER;

public class Suj {

	private String nom_sujet;

	private Group groupe;
	

	public Suj(String nom_sujet, Group groupe) {
		super();
		this.nom_sujet = nom_sujet;
		this.groupe = groupe;
	}
	
	public void setGroupe(Group g) {
		this.groupe = g;
	}

	public String getNom_sujet() {
		return nom_sujet;
	}

	public void setNom_sujet(String nom_sujet) {
		this.nom_sujet = nom_sujet;
	}

	public Group getGroupe() {
		return groupe;
	}
}
