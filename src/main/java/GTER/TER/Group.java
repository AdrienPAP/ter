package GTER.TER;

import java.util.ArrayList;
import java.util.List;

public class Group {
	
	public Group(int numgroup, Suj sujetaffecté, List<Voe> listeVoeux) {
		super();
		this.numgroup = numgroup;
		this.sujetaffecté = sujetaffecté;
		ListeVoeux = listeVoeux;
	}

	public int getNumgroup() {
		return numgroup;
	}

	public void setNumgroup(int numgroup) {
		this.numgroup = numgroup;
	}

	public Suj getSujetaffecté() {
		return sujetaffecté;
	}

	public void setSujetaffecté(Suj sujetaffecté) {
		this.sujetaffecté = sujetaffecté;
	}

	public List<Voe> getListeVoeux() {
		return ListeVoeux;
	}

	public void setListeVoeux(List<Voe> listeVoeux) {
		ListeVoeux = listeVoeux;
	}

	private int numgroup;
	private Suj sujetaffecté;
	private List<Voe> ListeVoeux = new ArrayList<>();
	
	public void setVoeu(Suj sujet, int priority) {
		Voe voeu = new Voe(sujet, priority, this);
		this.ListeVoeux.add(voeu);
	}
	
	public void setSujet(Suj s) {
		this.sujetaffecté = s;
		s.setGroupe(this);
	}
	
	public void echecaffectation () {
		
	}
}

